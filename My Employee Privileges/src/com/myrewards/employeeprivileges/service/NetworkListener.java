package com.myrewards.employeeprivileges.service;

public interface NetworkListener {
	void onRequestCompleted(String response, String errorString, int eventType);

}
