package com.myrewards.employeeprivileges.service;

public interface ServiceListener {
	public void onServiceComplete(Object response, int eventType);
}
